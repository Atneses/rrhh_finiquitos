pyodbc==4.0.24
wkhtmltopdf==0.2
tqdm=4.25.0
XlsxWriter=1.1.0
pdfkit==0.6.1
Jinja2==2.10