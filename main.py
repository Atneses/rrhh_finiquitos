# -*- coding: utf-8 -*-
import argparse
import datetime

from tqdm import tqdm

from modules.connection import Connection
from modules.excel import ExcelReport
from modules.report import SettlementReport

parser = argparse.ArgumentParser()

# Add arguments
parser.add_argument('end_date', help='Cuando sera la fecha de baja.')
parser.add_argument('--since_date', help='help')
parser.add_argument('--employee', help='Numero de empleado.', type=int)
parser.add_argument('--pdf', help='', action='store_true')
parser.add_argument('--list', nargs='+', help='')

# Parse and get arguments
args = parser.parse_args()
since_date = args.since_date
end_date = args.end_date
end_date = datetime.datetime.strptime(end_date, '%Y-%m-%d').date()
employee_numbers = args.list
employee_number = args.employee
generate_pdf = args.pdf

if not (employee_number and since_date):
    report = SettlementReport()
    ex = ExcelReport()
    if employee_number:
        c = Connection()
        employee = c.settlement_by_employee(employee_number, end_date)
        report.generate_report(employee, end_date)
        ex.generate_report([employee], end_date)

    elif employee_numbers:
        employees = []
        c = Connection()
        for number in tqdm(employee_numbers):
            e = c.settlement_by_employee(number, end_date)
            employees.append(e)

        if generate_pdf:
            for e in tqdm(employees):
                report.generate_report(e, end_date)
        ex.generate_report(employees, end_date)

    else:
        since_date = datetime.datetime.strptime(since_date, '%Y-%m-%d').date()
        print('Por fecha')
        c = Connection()
        print('Calculando...')
        employees = c.get_fired_employees_since(since_date, end_date)

        if generate_pdf:
            print('Generando reportes')
            for e in tqdm(employees):
                report.generate_report(e, end_date)

        print('Generando report general')
        ex.generate_report(employees, end_date)

else:
    print('Ingresa solo un parametro, --since_date o --employee')
