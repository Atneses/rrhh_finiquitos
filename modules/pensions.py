#!/usr/bin/python
# -*- coding: utf-8 -*-

import csv
from collections import OrderedDict

FILENAME = 'files/IPEJAL.csv'
EMPLOYEE = 'No. Empleado'
DESCRIPTION = ' DESCRIPCION'
IMPORT = ' IMPORTE'

# region
FILE_NOMINA = 'files/nomina.csv'
D007 = 'D007 - IMSS'
D011 = 'D011 - P.Alim. CF y Bruto'
D035 = 'D035 - P.Alim.S/Neto'
D042 = 'D042 - PA Desp Elect'
D023 = 'D023 - Seg de Veh'
D043 = 'D043 - Fondo de Ahorro Voluntario'
D036 = 'D036 - Adelanto a cta de Prestaciones'
D028 = 'D028 - Otras deducc'
D029 = 'D029 - Otras Retenciones'
D009 = 'D009 - Pmo Caja'
# endregion


class DeductionsCSV(object):

    def get_pension(self, employee):
        with open(FILENAME) as csv_file:
            reader = csv.DictReader(csv_file)

            pensions_deductions = []
            for row in reader:
                if int(row[EMPLOYEE]) == employee:
                    pensions_deductions.append(row)

        return pensions_deductions

    def get_deductions(self, employee):

        with open(FILE_NOMINA) as csv_nomina:
            reader = csv.DictReader(csv_nomina)

            for row in reader:
                try:
                    if employee == int(row['Empleado']):
                        del row['Empleado']
                        del row['Nombre Empleado']
                        del row['Centro Costo']
                        del row['Desc. Centro Costo']
                        del row['Desc. Puesto']

                        row[D007] = float(row[D007])
                        if row[D007] == 0:
                            del row[D007]

                        row[D011] = float(row[D011])
                        if row[D011] == 0:
                            del row[D011]

                        row[D035] = float(row[D035])
                        if row[D035] == 0:
                            del row[D035]

                        row[D042] = float(row[D042])
                        if row[D042] == 0:
                            del row[D042]

                        row[D023] = float(row[D023])
                        if row[D023] == 0:
                            del row[D023]

                        row[D043] = float(row[D043])
                        if row[D043] == 0:
                            del row[D043]

                        row[D036] = float(row[D036])
                        if row[D036] == 0:
                            del row[D036]

                        row[D028] = float(row[D028])
                        if row[D028] == 0:
                            del row[D028]

                        row[D029] = float(row[D029])
                        if row[D029] == 0:
                            del row[D029]

                        row[D009] = float(row[D009])
                        if row[D009] == 0:
                            del row[D009]
                        return row
                except ValueError as error:
                    return OrderedDict()
            return []
