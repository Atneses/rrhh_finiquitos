#!/usr/bin/python
# -*- coding: utf-8 -*-
import math


def units(x):
    if x == 0:
        unit = "cero"
    if x == 1:
        unit = "un"
    if x == 2:
        unit = "dos"
    if x == 3:
        unit = "tres"
    if x == 4:
        unit = "cuatro"
    if x == 5:
        unit = "cinco"
    if x == 6:
        unit = "seis"
    if x == 7:
        unit = "siete"
    if x == 8:
        unit = "ocho"
    if x == 9:
        unit = "nueve"
    return unit


def teens(x):
    if x == 0:
        teen_name = "diez"
    if x == 1:
        teen_name = "once"
    if x == 2:
        teen_name = "doce"
    if x == 3:
        teen_name = "trece"
    if x == 4:
        teen_name = "catorce"
    if x == 5:
        teen_name = "quince"
    return teen_name


def tens(x):
    if x == 1:
        tens_name = "diez"
    if x == 2:
        tens_name = "veinte"
    if x == 3:
        tens_name = "treinta"
    if x == 4:
        tens_name = "cuarenta"
    if x == 5:
        tens_name = "cincuenta"
    if x == 6:
        tens_name = "sesenta"
    if x == 7:
        tens_name = "setenta"
    if x == 8:
        tens_name = "ochenta"
    if x == 9:
        tens_name = "noventa"
    return tens_name


def tertiary(num):
    number = str(num)
    if len(number) == 1:
        number = '00' + number
    if len(number) == 2:
        number = '0' + number
    a = int(number[0])
    b = int(number[1])
    c = int(number[2])
#       print a, b, c
    if a == 0:
        if b == 0:
            result = units(c)
            return result
        elif b == 1:
            if 0 <= c <= 5:
                result = teens(c)
                return result
            elif 6 <= c <= 9:
                result = tens(b) + ' y ' + units(c)
                return result
        elif b == 2:
            if c == 0:
                result = 'veinte'
                return result
            elif 0 < c <= 9:
                result = 'veinti ' + units(c)
                return result
        elif 3 <= b <= 9:
            if c == 0:
                result = tens(b)
                return result
            if 1 <= c <= 9:
                result = tens(b) + ' y ' + units(c)
                return result
    if a == 1:
        if b == 0:
            if c == 0:
                result = 'cien'
                return result
            elif 0 < c <= 9:
                result = 'ciento ' + units(c)
                return result
        elif b == 1:
            if 0 <= c <= 5:
                result = 'ciento ' + teens(c)
                return result
            elif 6 <= c <= 9:
                result = 'ciento ' + tens(b) + ' y ' + units(c)
                return result
        elif b == 2:
            if c == 0:
                result = 'ciento veinte'
                return result
            elif 0 < c <= 9:
                result = 'ciento veinti ' + units(c)
                return result
        elif 3 <= b <= 9:
            if c == 0:
                result = 'ciento ' + tens(b)
                return result
            elif 0 < c <= 9:
                result = 'ciento ' + tens(b) + ' y ' + units(c)
                return result

    elif 2 <= a <= 9:
        if a == 5:
            prefix = 'quinientos '
        elif a == 7:
            prefix = 'setecientos '
        elif a == 9:
            prefix = 'novecientos '
        else:
            prefix = units(a) + ' cientos '
        if b == 0:
            if c == 0:
                result = prefix
                return result
            elif 0 < c <= 9:
                result = prefix + units(c)
                return result
        elif b == 1:
            if 0 <= c <= 5:
                result = prefix + teens(c)
                return result
            elif 6 <= c <= 9:
                result = prefix + tens(b) + ' y ' + units(c)
                return result
        elif b == 2:
            if c == 0:
                result = prefix + ' veinte'
                return result
            elif 0 < c <= 9:
                result = prefix + ' veinti ' + units(c)
                return result
        elif 3 <= b <= 9:
            if c == 0:
                result = prefix + tens(b)
                return result
            elif 0 < c <= 9:
                result = prefix + tens(b) + ' y ' + units(c)
                return result


def number_to_text(num):
    result = ''
    number = str(num)

    if len(number) == 1:
        number = '00000000' + number
    if len(number) == 2:
        number = '0000000' + number
    if len(number) == 3:
        number = '000000' + number
    if len(number) == 4:
        number = '00000' + number
    if len(number) == 5:
        number = '0000' + number
    if len(number) == 6:
        number = '000' + number
    if len(number) == 7:
        number = '00' + number
    if len(number) == 8:
        number = '0' + number

    for i in [0, 3, 6]:
        var = number[i] + number[i + 1] + number[i + 2]
        if int(var) != 0:
            res = tertiary(var)
            if i == 0:
                result = res + " millones "
            elif i == 3:
                result = result+res+" mil "
            elif i == 6:
                result = result+res
    return " ".join(result.capitalize().split())


def float_to_text(number):
    number_s = str('{0:.2f}'.format(number))
    x = float(number_s)
    f = int(number_s.split('.')[1])

    number_split = math.modf(x)
    i_s = number_to_text(int(number_split[1]))
    f_s = number_to_text(f)
    print('')
    number_test = '{integer} con {decimal} centavos'.format(integer=i_s, decimal=f_s).capitalize()
    return number_test
