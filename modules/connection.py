#!/usr/bin/python
# -*- coding: utf-8 -*-
import datetime
import pyodbc

from tqdm import tqdm

from modules.pensions import DeductionsCSV

# region Constants for table empleados
DESCRIPCION = 'descripcion'
TOTAL = 'total'
EMPLOYEE = 'empleado'
NAME = 'nombre'
CURP = 'cuenta_individual'
RFC = 'curp'
POSITION = 'puesto'
ADSCRIPTION = 'adscripcion'
ID = 'id'
FECHA_BAJA = 'fecha_baja'
FECHA_ANTIGUEDAD = 'fecha_antiguedad'
NOMINA = 'nomina'
SEXO = 'sexo'
APLICA_DIA_MADRE = 'aplica_dia_madre'
NIVEL_ESTRUCTURA = 'id_nivel_estructura'
APLICAR_IMSS = 'aplicar_imss'
NIVEL = 'nivel'
# endregion

# region Functionary day constants.
FUNCTIONARY_DAY = 'functionary_day'
FUNCTIONARY_EXEMPT = 'functionary_exempt'
FUNCTIONARY = 'Funcionario'
FIREFIGHTER = 'Bombero'
MOTHER = 'Madre'
POLICE = 'Policia'
IS = 'is'
# endregion

# region calcs
BONUS_YEAR = 'bonus_year'
PRIME = 'prime'
VACATION = 'vacation'
QUINCENA = 'quincena'
Q_SALARY = 'q_salary'
Q_PANTRY = 'q_pantry'
Q_COMPLEMENT = 'q_complement'
FUNCTIONARY_DAY_TAX = 'functionary_day_tax'
FACTOR = 'factor'
Q_TRANSPORT = 'q_transport'
Q_COMPLEMENTO_TAX = 'q_complemento_tax'
Q_COMPLEMENTO = 'q_complemento'
Q_QUINCENA_TAX = 'q_quincena_tax'
Q_QUINCENA = 'q_quincena'
VACATION_LAST_YEAR_TAX = 'vacation_last_year_tax'
VACATION_THIS_YEAR_TAX = 'vacation_this_year_tax'
PRIME_TAX = 'prime_tax'
BONUS_YEAR_TAX = 'bonus_year_tax'
# endregion

# region Nominas Ids
FIRST_LEVEL = 6
# endregion

# region Vacation constants
THIS_YEAR = 'this_year'
TAKEN = 'taken'
LAST_YEAR = 'last_year'
TAKEN_LAST = 'taken_last'
FAULTS_LAST_YEAR = 'faults_last_year'
FAULTS_THIS_YEAR = 'faults_this_year'
# endregion

MINIMUM_SALARY = 'salario_minimo'
FAULTS = 'faults'

# region Taxable constants
BONUS_TAXABLE = 'bonus_taxable'
BONUS_EXEMPT = 'bonus_exempt'
PRIME_TAXABLE = 'prime_taxable'
PRIME_EXEMPT = 'prime_exempt'
# endregion

# region Integrated salary constants
INTEGRATED_SALARY = 'integrated_salary'
MONTHLY_SALARY = 'monthly_salary'
DIARY_SALARY = 'diary_salary'
HOURLY_SALARY = 'hourly_salary'
PANTRY = 'pantry'
COMPLEMENT = 'complement'
MONTHLY_COMPLEMENT = 'monthly_complement'
TRANSPORT = 'transport'
CALCS = 'calcs'
# endregion

# region Pension deductions constants
PENSION_DEDUCTIONS = 'pension_deductions'
NOMINA_DEDUCTIONS = 'nomina_deductions'
# endregion

EXCLUDED_POSITIONS = ['presidente municipal', 'regidor', 'sindico', 'comisario']


class Connection(object):

    def __init__(self, server='192.168.10.31', database='Eslabon', user_id='sa', pwd='2018ESLABON'):
        self.conn = pyodbc.connect('DRIVER={{SQL Server}};SERVER={0};DATABASE={1};Integrated Security:true'.format(
            server,
            database,
            user_id,
            pwd
        ))
        self.cursor = self.conn.cursor()
        self.uma = self.get_uma()

    def get_faults(self, employee, init_date, end_date):
        """
        Get the faults, licenses, suspensions of the employee between the given
        dates.

        :param employee: Dictionary object of employee.
        :param init_date: Start date of the faults, date object.
        :param end_date: End date of the faults, date object.
        :return: Count of total faults between the two given dates.
        """

        init_date = '{0}-{1}-{2}'.format(init_date.year, init_date.day, init_date.month)
        end_date = '{0}-{1}-{2}'.format(end_date.year, end_date.day, end_date.month)

        query = '''
            DECLARE @out float;
            EXEC [dbo].[pc_suma_ausentismos] {0}, {1}, {2}, '{3}', '{4}', {5}, @dias_ausentismo = @out OUTPUT
            SELECT @out as Faltas
        '''.format(
            1000,
            employee[NOMINA],
            employee[ID],
            init_date,
            end_date,
            employee[APLICAR_IMSS]
        )

        result = self.conn.execute(query)
        s = []
        while result.nextset():
            s.append(result.fetchall())

        faults = s[2][0][0]
        return faults

    def get_v_days_taken(self, employee_id, init_date):
        """
        Get the taken days of vacations for the given employee_id starting in
        the given 'init_date'.

        :param employee_id: Employee id number in the database, different from
            the employee number.
        :param init_date: Start date, date object.
        :return: Count of the taken days of vacations.
        """

        init_date_s = '{0}-{1}-{2}'.format(init_date.year, init_date.day, init_date.month)
        query = '''
          SELECT
            id
            ,sum(dias_descanso) AS [dias_descanso]
          FROM
            vacaciones_empleado
          WHERE
            id = ?
            AND fecha_inicio >= ?
          GROUP BY
            id
        '''

        result = self.cursor.execute(query, employee_id, init_date_s).fetchone()
        if result:
            days = int(result[1])
        else:
            days = 0
        return days

    def get_v_taken_last_year(self, employee_id):
        init_date = '{0}-01-07'.format(datetime.datetime.now().year - 1)
        end_date = '{0}-31-12'.format(datetime.datetime.now().year - 1)

        query = '''
          SELECT
            id
            ,SUM(dias_descanso) AS [dias_descanso]
          FROM
            vacaciones_empleado
          WHERE
            id = ?
            AND fecha_inicio >= ?
            AND fecha_inicio < ?
          GROUP BY
            id
        '''
        result = self.conn.execute(query, employee_id, init_date, end_date).fetchone()
        if result:
            return result[1]
        return 0

    def get_integrated_salary(self, employee_id, end_date):
        """
        Get the integrated salary of the given 'employee_id'.

        :param employee_id: Employee id number in the database, different from
            the employee number.
        :param end_date: Day they were discharged, date object.
        :return: Tuple of dictionaries, with the integrated salary if position 0
            and monthly complement and monthly salary in position 1.
        """

        end_date_s = '{0}-{1}-{2}'.format(end_date.year, end_date.day, end_date.month)
        query_salary = '''
          SELECT
          TOP 1
          id,
          sueldo_mensual,
          sueldo_diario,
          sueldo_hora
        FROM
          empleados_sueldos
        WHERE
          id = ?
          AND fecha_vigencia_hasta = ?
        ORDER BY
          fecha_vigencia_hasta DESC
      '''
        result = self.cursor.execute(query_salary, employee_id, end_date_s).fetchone()
        monthly_salary = result[1]
        diary_salary = result[2]

        query_transport = '''
          SELECT
            id,
            DespensaX,
            ayuda_transporte
          FROM
            campos_dinamicos
          WHERE
            id = ?
        '''
        result = self.cursor.execute(query_transport, employee_id).fetchone()
        pantry = result[1]
        transport = result[2]

        query_complement = '''
          SELECT
            id,
            importe1
          FROM
            captura
          WHERE
            id = ?
            AND dip LIKE 'P003'
        '''
        result = self.cursor.execute(query_complement, employee_id).fetchone()
        if result:
            complement = float(result[1]) / 15
        else:
            complement = 0

        return [{
            DIARY_SALARY: diary_salary,
            PANTRY: pantry / 15,
            TRANSPORT: transport / 15,
            COMPLEMENT: complement
        }, {
            MONTHLY_COMPLEMENT: complement * 30,
            MONTHLY_SALARY: monthly_salary
        }]

    def calculate_quinquennium(self, init_date, end_date, integrated):
        """
        Calculate the quinquennium for the integrated salary always that in
            have more than 5 years antiquity.
        :param init_date: Start date, date object.
        :param end_date: End date, date object.
        :param integrated: Dictionary with the values that make up the
            integrated salary
        :return: Return quinquennium or 0 if it has less or five years.
        """
        days = (end_date - init_date).days
        years = int(days / 365)
        quinquennium = years / 5
        if quinquennium > 4:
            return ((sum(integrated.values())) / 2) * quinquennium
        return 0

    def police_day_date(self):
        """
        Get the current year police day date.

        :return: Date object.
        """
        return datetime.date(self.get_current_year(), 1, 1)

    def mother_day_date(self):
        """
        Get the current year mother day date.

        :return: Date object.
        """
        return datetime.date(self.get_current_year(), 5, 10)

    def firefighter_day_date(self):
        """
        Get the current year firefighter day date.

        :return: Date object.
        """
        return datetime.date(self.get_current_year(), 8, 22)

    def functionary_day_date(self):
        """
        Get the current year functionary day date.

        :return: Date object.
        """
        return datetime.date(self.get_current_year(), 9, 28)

    def get_current_year(self):
        """
        Get the current year.

        :return: Integer representing the current year.
        """
        return datetime.datetime.now().year

    def get_previous_year(self):
        """
        Get the previous year.

        :return: Integer representing the previous year.
        """
        return datetime.datetime.now().year - 1

    def get_uma(self):
        """
        Get the current UMA.

        :return: Current UMA, float.
        """
        query = '''
          SELECT
            zona
            ,salario_minimo
          FROM
            zonas
          WHERE
            descripcion like '%UMA%'
            AND fecha_vigencia_hasta is null
        '''

        result = self.conn.execute(query)
        columns = [column[0] for column in result.description]

        uma = dict(zip(columns, result.fetchone()))
        return uma[MINIMUM_SALARY]

    def get_articulo133(self, salary, table=1):
        query = '''
          SELECT
            tp.descripcion
            ,ar.limite_inferior
            ,ar.limite_superior
            ,ar.cuota_fija
            ,ar.porcentaje
            ,ar.fecha_vigencia_desde
            ,ar.fecha_vigencia_hasta
          FROM
            articulo113 ar
            join tablas_impuesto tp ON tp.tipo_tabla = ar.tipo_tabla 
          WHERE
            fecha_vigencia_desde >= '2018-01-01'
            AND ar.tipo_tabla = ?
            AND ? >= limite_inferior
            AND ? <= limite_superior
        '''
        results = self.conn.execute(query, table, salary, salary)
        conlumns = [column[0] for column in results.description]
        art133 = dict(zip(conlumns, results.fetchone()))
        return art133

    def get_monthly_factor(self, sueldo_complemento):
        # calulo de factor ISR (sueldo + complemento) - limite inferior de la tabla 113 =  excedente
        tax = self.get_articulo133(sueldo_complemento)

        excedent = sueldo_complemento - tax['limite_inferior']
        excedent_percentage = tax['porcentaje']
        tax_marginal = excedent * (excedent_percentage / 100)
        cuota_fija = tax['cuota_fija']
        imp_ant_subs = tax_marginal + cuota_fija
        factor = imp_ant_subs / sueldo_complemento
        return factor

    def get_quincenal_factor(self, sueldo_complemento):
        tax = self.get_articulo133(sueldo_complemento, table=3)

        excedent = sueldo_complemento - tax['limite_inferior']
        excedent_percentage = tax['porcentaje']
        tax_marginal = excedent * (excedent_percentage / 100)
        cuota_fija = tax['cuota_fija']
        imp_ant_subs = tax_marginal + cuota_fija

        return imp_ant_subs

    def get_employment_subsidy(self, quinceal_salary):
        query = '''
        SELECT
          subsidio_para_empleado
        FROM
            [dbo].[subsidio_para_empleo]
        WHERE
            tipo_tabla = 5
            and ? >= limite_inferior
            and ? <= limite_superior

        '''

        results = self.conn.execute(query, quinceal_salary, quinceal_salary).fetchone()
        if results:
            return results[0]
        return 0

    def is_burocrata_paid(self, id_employee):
        year = self.get_current_year()
        init_date_s = '{0}-{1}-{2}'.format(year, 1, 1)
        end_date_s = '{0}-{1}-{2}'.format(year, 31, 12)
        query = '''
        SELECT
          id
          , dip
          , fecha_mvto
          , aplicado
        FROM
          recibos
        WHERE
          id = ?
        AND fecha_mvto >= ?
        AND fecha_mvto <= ?
        AND (
            dip LIKE 'P016'
            OR dip LIKE 'P017'
            OR dip LIKE 'P018'
            OR dip LIKE 'P015'
            )
        '''

        results = self.conn.execute(query, id_employee, init_date_s, end_date_s).fetchall()
        if len(results) > 0:
            return True
        return False

    # region Calculations
    def cal_functionary_day(self, employee, integrated_salary, end_date):
        police_paysheet = [2, 7, 19, 20]
        firefighter = [1000903, 1000905]
        number_days = 15 / 365
        # region TODO Caso especial por fin de administracion
        end_date = datetime.date(2018, 9, 28)
        # endregion
        if employee[NOMINA] in police_paysheet:
            y = (end_date - employee[FECHA_ANTIGUEDAD].date()).days
            if y >= 365:
                if end_date > self.functionary_day_date():
                    init_date = datetime.date(self.get_current_year(), 1, 2)
                else:
                    init_date = datetime.date(self.get_previous_year(), 1, 2)
                faults = self.get_faults(employee, init_date, end_date)
                days = (end_date - init_date).days + 1 - faults
                days = (days * number_days)
                if days > 15:
                    days = 15

                to_pay = days * sum(integrated_salary.values())
                return {FUNCTIONARY_DAY: to_pay, IS: POLICE, FAULTS: faults}
            else:
                faults = self.get_faults(employee, employee[FECHA_ANTIGUEDAD], end_date)
                days = (end_date - employee[FECHA_ANTIGUEDAD].date()).days + 1 - faults
                days = (days * number_days)
                if days > 15:
                    days = 15

                to_pay = days * sum(integrated_salary.values())
                return {FUNCTIONARY_DAY: to_pay, IS: POLICE, FAULTS: faults}

        elif employee[SEXO] == 0 and employee[APLICA_DIA_MADRE] == 1:
            y = (end_date - employee[FECHA_ANTIGUEDAD].date()).days
            if y >= 365:
                if end_date > self.functionary_day_date():
                    init_date = datetime.date(self.get_current_year(), 5, 11)
                else:
                    init_date = datetime.date(self.get_previous_year(), 5, 11)
                faults = self.get_faults(employee, init_date, end_date)
                days = (end_date - init_date).days + 1 - faults
                days = (days * number_days)
                if days > 15:
                    days = 15

                to_pay = days * sum(integrated_salary.values())
                return {FUNCTIONARY_DAY: to_pay, IS: MOTHER, FAULTS: faults}
            else:
                faults = self.get_faults(employee, employee[FECHA_ANTIGUEDAD], end_date)
                days = (end_date - employee[FECHA_ANTIGUEDAD].date()).days + 1 - faults
                days = (days * number_days)
                if days > 15:
                    days = 15

                to_pay = days * sum(integrated_salary.values())
                return {FUNCTIONARY_DAY: to_pay, IS: MOTHER, FAULTS: faults}

        elif employee[NIVEL_ESTRUCTURA] in firefighter:
            y = (end_date - employee[FECHA_ANTIGUEDAD].date()).days
            if y >= 365:
                if end_date > self.functionary_day_date():
                    init_date = datetime.date(self.get_current_year(), 8, 23)
                else:
                    init_date = datetime.date(self.get_previous_year(), 8, 23)
                faults = self.get_faults(employee, init_date, end_date)
                days = (end_date - init_date).days + 1 - faults
                days = (days * number_days)
                if days > 15:
                    days = 15

                to_pay = days * sum(integrated_salary.values())
                return {FUNCTIONARY_DAY: to_pay, IS: FIREFIGHTER, FAULTS: faults}
            else:
                faults = self.get_faults(employee, employee[FECHA_ANTIGUEDAD], end_date)
                days = (end_date - employee[FECHA_ANTIGUEDAD].date()).days + 1 - faults
                days = (days * number_days)
                if days > 15:
                    days = 15

                to_pay = days * sum(integrated_salary.values())
                return {FUNCTIONARY_DAY: to_pay, IS: FIREFIGHTER, FAULTS: faults}

        else:
            y = (end_date - employee[FECHA_ANTIGUEDAD].date()).days
            if y >= 365:
                if end_date > self.functionary_day_date():
                    init_date = datetime.date(self.get_current_year(), 9, 29)
                else:
                    init_date = datetime.date(self.get_previous_year(), 9, 29)
                faults = self.get_faults(employee, init_date, end_date)
                days = (end_date - init_date).days + 1 - faults
                days = (days * number_days)
                if days > 15:
                    days = 15

                to_pay = days * sum(integrated_salary.values())
                return {FUNCTIONARY_DAY: to_pay, IS: FUNCTIONARY, FAULTS: faults}
            else:
                faults = self.get_faults(employee, employee[FECHA_ANTIGUEDAD], end_date)
                days = (end_date - employee[FECHA_ANTIGUEDAD].date()).days + 1 - faults
                days = (days * number_days)
                if days > 15:
                    days = 15

                to_pay = days * sum(integrated_salary.values())
                return {FUNCTIONARY_DAY: to_pay, IS: FUNCTIONARY, FAULTS: faults}

    def cal_year_bonus(self, employee, integrated_salary, end_date):
        init_date = datetime.date(datetime.datetime.now().year, 1, 1)
        faults = self.get_faults(employee, init_date, end_date)
        if employee[FECHA_ANTIGUEDAD].date() > init_date:
            init_date = employee[FECHA_ANTIGUEDAD].date()

        days = (end_date - init_date).days + 1 - faults
        x = (50 / 365) * days

        total = x * sum(integrated_salary.values())
        bonus_exempt = self.uma * 30
        if total >= bonus_exempt:
            bonus_taxable = total - bonus_exempt
        else:
            bonus_exempt = total
            bonus_taxable = 0
        return {
            BONUS_EXEMPT: bonus_exempt,
            BONUS_TAXABLE: bonus_taxable,
            TOTAL: bonus_exempt + bonus_taxable,
            FAULTS: faults
        }

    def get_prima_last_exempt(self, id_e):
        d = datetime.date(datetime.datetime.now().year, 3, 31)
        s_d = '{0}-{1}-{2}'.format(d.year, d.day, d.month)
        query = '''
              SELECT
                id
                ,exento
              FROM
                recibos
              WHERE
                dip = 'P013'
                AND fecha_mvto = ?
                AND id = ?
              ORDER BY
                fecha_registro
            '''
        result = self.conn.execute(query, s_d, id_e).fetchone()
        if result:
            return float(result[1])
        else:
            return 0.0

    def cal_prima_vacacional(self, employee, integrated_salary, end_date):
        init_date = datetime.date(self.get_current_year(), 4, 1)
        faults = self.get_faults(employee, init_date, end_date)
        if employee[FECHA_ANTIGUEDAD].date() > init_date:
            init_date = employee[FECHA_ANTIGUEDAD].date()

        days = (end_date - init_date).days + 1 - faults
        x = 5 / 365

        exempt = self.uma * 15

        prime = (days * x) * sum(integrated_salary.values())

        if prime >= exempt:
            last_exempt = self.get_prima_last_exempt(employee[ID])
            exempt = exempt - last_exempt

            if exempt > 0:
                prime_exempt = exempt
                prime_taxable = prime - exempt
            else:
                prime_exempt = 0
                prime_taxable = prime
        else:
            prime_exempt = prime
            prime_taxable = 0

        return {
            PRIME_EXEMPT: prime_exempt,
            PRIME_TAXABLE: prime_taxable,
            TOTAL: prime_exempt + prime_taxable,
            FAULTS: faults
        }

    def cal_vacation(self, employee, integrated_salary, faults_this_year, end_date):
        current_date = end_date
        ds = (current_date - employee[FECHA_ANTIGUEDAD].date()).days

        if ds >= 365:
            init_date = datetime.date(datetime.datetime.now().year, 1, 1)
            faults_this_year = self.get_faults(employee, init_date, end_date)
            to_pay = (end_date - init_date).days + 1
            factor_this_year = 20 / 365
            factor_last_year = 10 / 365
            taken = self.get_v_days_taken(employee[ID], init_date)
            to_pay = (to_pay * factor_this_year) - taken - faults_this_year
            # TODO check condition when to_pay is lower than zero, 0.
            i_f = datetime.date(datetime.datetime.now().year - 1, 7, 1)
            e_f = datetime.date(datetime.datetime.now().year - 1, 12, 31)

            taken_last = self.get_v_taken_last_year(employee[ID])

            x = (e_f - i_f).days - 0.5

            faults_last_year = self.get_faults(employee, i_f, e_f)
            to_pay_last = (x * factor_last_year) * sum(integrated_salary.values()) - faults_last_year

            this_year = to_pay * sum(integrated_salary.values())
            last_year = to_pay_last
            return {
                THIS_YEAR: this_year,
                TAKEN: taken,
                LAST_YEAR: last_year,
                TAKEN_LAST: taken_last,
                FAULTS_THIS_YEAR: faults_this_year,
                FAULTS_LAST_YEAR: faults_last_year
            }

        else:
            # Menos de un año
            faults_this_year = self.get_faults(employee, employee[FECHA_ANTIGUEDAD], end_date)
            taken = self.get_v_days_taken(employee[ID], end_date)
            init_date = employee[FECHA_ANTIGUEDAD].date()
            dd = (end_date - init_date).days + 1
            factor_this_year = 20 / 365
            to_pay = (dd * factor_this_year) - faults_this_year - taken

            this_year = to_pay * sum(integrated_salary.values())
            return {
                THIS_YEAR: this_year,
                TAKEN: taken,
                LAST_YEAR: 0.0,
                TAKEN_LAST: 0,
                FAULTS_THIS_YEAR: faults_this_year,
                FAULTS_LAST_YEAR: 0
            }

    # endregion

    def get_fired_employees_since(self, since_date, end_date):
        since_date = '{0}-{1}-{2}'.format(since_date.year, since_date.day, since_date.month)
        query = '''
                  SELECT    
                     E.id
                    ,E.fecha_baja
                    ,E.fecha_antiguedad
                    ,E.nomina
                    ,E.aplicar_imss
                    ,E.estatus
                    ,E.empleado
                    ,N.descripcion
                    ,R.nombre + ' ' + R.paterno + ' ' + R.materno AS [nombre]
                    ,R.sexo
                    ,R.curp
                    ,R.cuenta_individual
                    ,CD.aplica_dia_madre
                    ,MAX(EE.id_nivel_estructura) as [id_nivel_estructura]
                    ,PU.descripcion as [puesto]
                    ,NE.descripcion as [adscripcion]
                    ,NE.nivel
                    FROM
                      empleados E
                      JOIN nominas N on E.nomina = N.nomina
                      JOIN recursos R on R.id = E.id
                      JOIN campos_dinamicos CD ON CD.id = E.id
                      JOIN estructuras_empleado EE ON EE.id = E.id
                      JOIN empleados_plazas ep ON ep.id = e.id
                      JOIN plazas p ON p.plaza = ep.plaza
                      JOIN puestos pu ON pu.id_nivel_puesto = p.id_nivel_puesto
                      JOIN niveles_estructura NE ON NE.id_nivel_estructura = EE.id_nivel_estructura
                    WHERE
                      E.fecha_antiguedad >= ?
                      AND (
                          E.nomina = 1
                          OR E.nomina = 5
                          OR E.nomina = 2
                          OR E.nomina = 7
                          OR E.nomina = 19
                          OR E.nomina = 20
                          OR E.nomina = 6
                          )
                      /*AND (
                          E.estatus = 1
                          OR E.estatus = 3
                          )*/
                      AND EP.fecha_vigencia_hasta is null
                      AND EE.fecha_vigencia_hasta is null
                      AND NE.id_nivel_estructura >= 1000662

                    GROUP BY
                       E.id
                      ,E.fecha_baja
                      ,E.fecha_antiguedad
                      ,E.nomina
                      ,E.aplicar_imss
                      ,E.estatus
                      ,E.empleado
                      ,N.descripcion
                      ,R.nombre
                      ,R.paterno
                      ,R.materno
                      ,R.sexo
                      ,R.curp
                      ,R.cuenta_individual
                      ,CD.aplica_dia_madre
                      ,PU.descripcion
                      ,NE.descripcion
                      ,NE.nivel
                    ORDER BY
                      E.nomina
                '''
        results = self.cursor.execute(query, since_date)

        columns = [column[0] for column in results.description]

        employees = []

        for row in results:
            employees.append(dict(zip(columns, row)))

        print('Cantidad de empleados: ', len(employees))
        for_report = []
        for e in tqdm(employees):
            for_report.append(self.settlement_by_employee(e[EMPLOYEE], end_date))
        return for_report

    def settlement_by_employee(self, employee_number, end_date):
        end_date_s = '{0}-{1}-{2}'.format(end_date.year, end_date.day, end_date.month)
        query = '''
          SELECT 
             E.id
            ,E.fecha_baja
            ,E.fecha_antiguedad
            ,E.nomina
            ,E.aplicar_imss
            ,E.estatus
            ,E.empleado
            ,N.descripcion
            ,R.paterno + ' ' + R.materno + ' ' + R.nombre AS [nombre]
            ,R.sexo
            ,R.curp
            ,R.cuenta_individual
            ,CD.aplica_dia_madre
            ,MAX(EE.id_nivel_estructura) as [id_nivel_estructura]
            ,PU.descripcion as [puesto]
            ,NE.descripcion as [adscripcion]
            ,NE.nivel
            FROM
              empleados E
              JOIN nominas N on E.nomina = N.nomina
              JOIN recursos R on R.id = E.id
              JOIN campos_dinamicos CD ON CD.id = E.id
              JOIN estructuras_empleado EE ON EE.id = E.id
              JOIN empleados_plazas ep ON ep.id = e.id
              JOIN plazas p ON p.plaza = ep.plaza
              JOIN puestos pu ON pu.id_nivel_puesto = p.id_nivel_puesto
              JOIN niveles_estructura NE ON NE.id_nivel_estructura = EE.id_nivel_estructura
            WHERE
              E.empleado = ?
              AND (
                  E.nomina = 1
                  OR E.nomina = 5
                  OR E.nomina = 2
                  OR E.nomina = 7
                  OR E.nomina = 19
                  OR E.nomina = 20
                  OR E.nomina = 6
                  )
              /*AND (
                  E.estatus = 1
                  OR E.estatus = 3
                  )*/
              AND EP.fecha_vigencia_hasta = ?
              AND EE.fecha_vigencia_hasta = ?
              AND NE.id_nivel_estructura >= 1000662

            GROUP BY
               E.id
              ,E.fecha_baja
              ,E.fecha_antiguedad
              ,E.nomina
              ,E.aplicar_imss
              ,E.estatus
              ,E.empleado
              ,N.descripcion
              ,R.nombre
              ,R.paterno
              ,R.materno
              ,R.sexo
              ,R.curp
              ,R.cuenta_individual
              ,CD.aplica_dia_madre
              ,PU.descripcion
              ,NE.descripcion
              ,NE.nivel
            ORDER BY
              E.nomina
        '''
        results = self.cursor.execute(query, employee_number, end_date_s, end_date_s)

        columns = [column[0] for column in results.description]
        try:
            employee = dict(zip(columns, results.fetchone()))
        except TypeError:
            print(employee_number)

        asdasdsd = self.get_integrated_salary(employee[ID], end_date)
        integrated_salary = asdasdsd[0]
        monthly_salary = asdasdsd[1]

        # Get quinquenio
        nominas = [
            'Nomina General',
            'Fortalecimiento',
            'Comisionados',
            'Comisionados Fortalecimiento',
            'Fortalecimiento Unidad de Analisis y Reaccion',
            'Escoltas Fortalecimiento'
        ]
        if employee['descripcion'] in nominas:
            integrated_salary['quinquennium'] = self.calculate_quinquennium(
                employee[FECHA_ANTIGUEDAD].date(),
                end_date,
                integrated_salary
            )
        else:
            integrated_salary['quinquennium'] = 0

        # Calculate 'Dia del funcionario', determinate if the employee if
        # police, female and mother, firefighter or bureaucrat.
        if employee[NOMINA] != FIRST_LEVEL\
                and not self.is_burocrata_paid(employee[ID])\
                and employee[POSITION].lower() not in EXCLUDED_POSITIONS:
            fd = self.cal_functionary_day(employee, integrated_salary, end_date)
        else:
            fd = {FUNCTIONARY_DAY: 0, IS: 'Funcionario', FAULTS: 0}

        bonus_year = self.cal_year_bonus(employee, integrated_salary, end_date)

        if employee[POSITION].lower() not in EXCLUDED_POSITIONS:
            prime = self.cal_prima_vacacional(employee, integrated_salary, end_date)
        else:
            prime = {
                PRIME_EXEMPT: 0,
                PRIME_TAXABLE: 0,
                TOTAL: 0,
                FAULTS: 0
            }

        if employee[POSITION].lower() not in EXCLUDED_POSITIONS:
            vacation = self.cal_vacation(employee, integrated_salary, 1, end_date)
        else:
            vacation = {
                THIS_YEAR: 0,
                TAKEN: 0,
                LAST_YEAR: 0.0,
                TAKEN_LAST: 0,
                FAULTS_THIS_YEAR: 0,
                FAULTS_LAST_YEAR: 0
            }

        q_salary = integrated_salary[DIARY_SALARY] * 15
        q_complement = integrated_salary[COMPLEMENT] * 15
        q_pantry = integrated_salary[PANTRY] * 15
        q = {Q_SALARY: q_salary, Q_COMPLEMENT: q_complement, Q_PANTRY: q_pantry}

        sueldo_complemento_quincenal = q[Q_SALARY] + q[Q_COMPLEMENT]
        employment_subsidy = self.get_employment_subsidy(sueldo_complemento_quincenal)

        sueldo_complemento_mensual = monthly_salary[MONTHLY_SALARY] + monthly_salary[MONTHLY_COMPLEMENT]

        imp_ant_subs = self.get_quincenal_factor(sueldo_complemento_quincenal)

        if imp_ant_subs >= employment_subsidy:
            imp_ant_subs = imp_ant_subs - employment_subsidy
        else:
            imp_ant_subs = 0

        factor_monthly = self.get_monthly_factor(sueldo_complemento_mensual)

        fd[FUNCTIONARY_DAY_TAX] = fd[FUNCTIONARY_DAY] * factor_monthly
        bonus_year[BONUS_YEAR_TAX] = bonus_year[BONUS_TAXABLE] * factor_monthly
        prime[PRIME_TAX] = prime[PRIME_TAXABLE] * factor_monthly

        if vacation[THIS_YEAR] >= 0:
            vacation[VACATION_THIS_YEAR_TAX] = vacation[THIS_YEAR] * factor_monthly
        else:
            vacation[VACATION_THIS_YEAR_TAX] = 0

        if vacation[LAST_YEAR]:
            vacation[VACATION_LAST_YEAR_TAX] = vacation[LAST_YEAR] * factor_monthly
        else:
            vacation[VACATION_LAST_YEAR_TAX] = 0

        quincena = {
            Q_QUINCENA: integrated_salary[DIARY_SALARY] * 15,
            Q_QUINCENA_TAX: imp_ant_subs,
            Q_COMPLEMENTO: integrated_salary[COMPLEMENT] * 15,
            Q_COMPLEMENTO_TAX: integrated_salary[COMPLEMENT] * 15 * factor_monthly,
            Q_PANTRY: integrated_salary[PANTRY] * 15,
            Q_TRANSPORT: integrated_salary[TRANSPORT] * 15
        }
        employee[INTEGRATED_SALARY] = integrated_salary
        employee[MONTHLY_SALARY] = monthly_salary

        deductions = DeductionsCSV()
        pensions_deductions = deductions.get_pension(employee[EMPLOYEE])
        nomina_deductions = deductions.get_deductions(employee[EMPLOYEE])

        employee[CALCS] = {
            FACTOR: factor_monthly,
            FUNCTIONARY_DAY: fd,
            BONUS_YEAR: bonus_year,
            PRIME: prime,
            VACATION: vacation,
            QUINCENA: quincena
        }
        employee[PENSION_DEDUCTIONS] = pensions_deductions
        employee[NOMINA_DEDUCTIONS] = nomina_deductions
        return employee
