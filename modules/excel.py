#!/usr/bin/python
# -*- coding: utf-8 -*-
import datetime

import xlsxwriter
from tqdm import tqdm

from modules.connection import NAME, POSITION, ADSCRIPTION, EMPLOYEE, RFC, CURP, QUINCENA, BONUS_YEAR, PRIME, VACATION, \
    THIS_YEAR, LAST_YEAR, FUNCTIONARY_DAY, FECHA_ANTIGUEDAD, CALCS, NIVEL, \
    DESCRIPCION, Q_QUINCENA, Q_COMPLEMENTO, Q_PANTRY, Q_TRANSPORT, TOTAL, BONUS_YEAR_TAX, FUNCTIONARY_DAY_TAX, \
    PRIME_TAX, VACATION_THIS_YEAR_TAX, VACATION_LAST_YEAR_TAX, Q_QUINCENA_TAX, PENSION_DEDUCTIONS, NOMINA_DEDUCTIONS, \
    TAKEN_LAST, TAKEN, FAULTS_LAST_YEAR, FAULTS_THIS_YEAR, FAULTS
from modules.pensions import DESCRIPTION, IMPORT, D007, D011, D035, D042, D023, D043, D036, D028, D029, D009
from modules.report import FOLDER

EXCEL_FILE = '{filename}-{sec}.xlsx'

# region Pensions constants
ABONO_A_PCP = 'Abono a PCP'
ABONO_A_PCV = 'Abono a PCV'
ABONO_A_PH = 'Abono a PH'
ABONO_A_PLMP = 'Abono a PLMP'
ABONO_A_PMP = 'Abono a PMP'
APORTACION_A_FONDO = 'Aportación a Fondo'
FONDO_DE_GARANTIA_PCV = 'Fondo de Garantía PCV'
FONDO_DE_GARANTIA_PH = 'Fondo de Garantía PH'
FONDO_DE_GARANTIA_PLMP = 'Fondo de Garantía PLMP'
RENTA = 'Renta'


# endregion


class ExcelReport(object):
    def __init__(self):
        pass

    def generate_report(self, employees, end_date):
        sec = datetime.datetime.now().microsecond
        workbook = xlsxwriter.Workbook(EXCEL_FILE.format(directory=FOLDER, filename='Reporte de finiquitos', sec=sec))
        sheet = workbook.add_worksheet('test')

        money_format = workbook.add_format({'num_format': '$#,##0.00'})
        date_format = workbook.add_format({'num_format': 'dd/mm/yyyy'})

        center_format = workbook.add_format({'align': 'center'})

        # region Personal
        sheet.merge_range(0, 0, 0, 10, 'INFORMACIÓN GENERAL', center_format)
        sheet.write_string(1, 0, u'Número de empleado')
        sheet.write_string(1, 1, u'Empleado')
        sheet.write_string(1, 2, u'Puesto')
        sheet.write_string(1, 3, u'CC')
        sheet.write_string(1, 4, u'Adscripción')
        sheet.write_string(1, 5, u'R.F.C.')
        sheet.write_string(1, 6, u'CURP')
        sheet.write_string(1, 7, u'Fecha de ingreso')
        sheet.write_string(1, 8, u'Fecha de baja')
        sheet.write_string(1, 9, u'Nomina')
        sheet.write_string(1, 10, u'Motivo de baja')
        # endregion

        # region Perceptions
        sheet.merge_range(0, 11, 0, 22, 'PERCEPCIONES', center_format)
        sheet.write_string(1, 11, u'Sueldo')
        sheet.write_string(1, 12, u'Complemento')
        sheet.write_string(1, 13, u'Vales')
        sheet.write_string(1, 14, u'Ayuda de Transporte')
        sheet.write_string(1, 15, u'Aguinaldo')
        sheet.write_string(1, 16, u'Prima vacacional')
        sheet.write_string(1, 17, u'Vacaciones 2017')
        sheet.write_string(1, 18, u'Vacaciones 2018')
        sheet.write_string(1, 19, u'Día del burócrata')
        # Subsidies
        sheet.write_string(1, 20, u'Subsidio municipal A')
        sheet.write_string(1, 21, u'Subsidio municipal SP')
        sheet.write_string(1, 22, u'Subsidio municipal PV')
        # endregion

        # region Deductions
        sheet.merge_range(0, 23, 0, 47, 'DEDUCCIONES', center_format)
        sheet.write_string(1, 23, u'I.S.R. Sueldo')
        sheet.write_string(1, 24, u'I.S.R. Vacaciones')
        sheet.write_string(1, 25, u'I.S.R. Aguinaldo')
        sheet.write_string(1, 26, u'I.S.R. Prima vacacional')
        sheet.write_string(1, 27, u'I.S.R. Día del burócrata')

        sheet.write_string(1, 28, u'Abono a PCP')
        sheet.write_string(1, 29, u'Abono a PCV')
        sheet.write_string(1, 30, u'Abono a PH')
        sheet.write_string(1, 31, u'Abono a PLMP')
        sheet.write_string(1, 32, u'Abono a PMP')
        sheet.write_string(1, 33, u'Aportación a Fondo')
        sheet.write_string(1, 34, u'Fondo de Garantía PCV')
        sheet.write_string(1, 35, u'Fondo de Garantía PH')
        sheet.write_string(1, 36, u'Fondo de Garantía PLMP')
        sheet.write_string(1, 37, u'Renta')

        sheet.write_string(1, 38, u'D007 - IMSS')
        sheet.write_string(1, 39, u'D011 - P.Alim. CF y Bruto')
        sheet.write_string(1, 40, u'D035 - P.Alim.S/Neto')
        sheet.write_string(1, 41, u'D042 - PA Desp Elect')
        sheet.write_string(1, 42, u'D023 - Seg de Veh')
        sheet.write_string(1, 43, u'D043 - Fondo de Ahorro Voluntario')
        sheet.write_string(1, 44, u'D036 - Adelanto a cta de Prestaciones')
        sheet.write_string(1, 45, u'D028 - Otras deducc')
        sheet.write_string(1, 46, u'D029 - Otras Retenciones')
        sheet.write_string(1, 47, u'D009 - Pmo Caja')
        # endregion

        # region Totals
        sheet.merge_range(0, 48, 0, 50, 'TOTALES', center_format)
        sheet.write_string(1, 48, 'Total percepciones')
        sheet.write_string(1, 49, 'Total deducciones')
        sheet.write_string(1, 50, 'Neto')

        sheet.write_string(1, 51, 'Vacactiones tomadas 2017')
        sheet.write_string(1, 52, 'Vacactiones tomadas 2018')
        sheet.write_string(1, 53, 'Faltas 2017')
        sheet.write_string(1, 54, 'Faltas 2018')
        sheet.write_string(1, 55, 'Faltas aguinaldo')
        sheet.write_string(1, 56, 'Faltas prima vacacional')
        sheet.write_string(1, 57, 'Faltas día del burócrata')
        # endregion

        r = 2
        for employee in tqdm(employees):
            # region Get pension deductions.
            abono_a_pcp = 0
            abono_a_pcv = 0
            abono_a_ph = 0
            abono_a_plmp = 0
            abono_a_pmp = 0
            aportacion_a_fondo = 0
            fondo_de_garantia_pcv = 0
            fondo_de_garantia_ph = 0
            fondo_de_garantia_plmp = 0
            renta = 0
            pension_deductions = employee[PENSION_DEDUCTIONS]

            nomina_deductions = employee[NOMINA_DEDUCTIONS]

            try:
                pension_1_concepto = pension_deductions[0][DESCRIPTION]
                pension_1_value = float(pension_deductions[0][IMPORT])

                if ABONO_A_PCP in pension_1_concepto:
                    abono_a_pcp = pension_1_value
                elif ABONO_A_PCV in pension_1_concepto:
                    abono_a_pcv = pension_1_value
                elif ABONO_A_PH in pension_1_concepto:
                    abono_a_ph = pension_1_value
                elif ABONO_A_PLMP in pension_1_concepto:
                    abono_a_plmp = pension_1_value
                elif ABONO_A_PMP in pension_1_concepto:
                    abono_a_pmp = pension_1_value
                elif APORTACION_A_FONDO in pension_1_concepto:
                    aportacion_a_fondo = pension_1_value
                elif FONDO_DE_GARANTIA_PCV in pension_1_concepto:
                    fondo_de_garantia_pcv = pension_1_value
                elif FONDO_DE_GARANTIA_PH in pension_1_concepto:
                    fondo_de_garantia_ph = pension_1_value
                elif FONDO_DE_GARANTIA_PLMP in pension_1_concepto:
                    fondo_de_garantia_plmp = pension_1_value
                elif RENTA in pension_1_concepto:
                    renta = pension_1_value

            except IndexError:
                pension_1_value = 0

            try:
                pension_2_concepto = pension_deductions[1][DESCRIPTION]
                pension_2_value = float(pension_deductions[1][IMPORT])

                if ABONO_A_PCP in pension_2_concepto:
                    abono_a_pcp = pension_2_value
                elif ABONO_A_PCV in pension_2_concepto:
                    abono_a_pcv = pension_2_value
                elif ABONO_A_PH in pension_2_concepto:
                    abono_a_ph = pension_2_value
                elif ABONO_A_PLMP in pension_2_concepto:
                    abono_a_plmp = pension_2_value
                elif ABONO_A_PMP in pension_2_concepto:
                    abono_a_pmp = pension_2_value
                elif APORTACION_A_FONDO in pension_2_concepto:
                    aportacion_a_fondo = pension_2_value
                elif FONDO_DE_GARANTIA_PCV in pension_2_concepto:
                    fondo_de_garantia_pcv = pension_2_value
                elif FONDO_DE_GARANTIA_PH in pension_2_concepto:
                    fondo_de_garantia_ph = pension_2_value
                elif FONDO_DE_GARANTIA_PLMP in pension_2_concepto:
                    fondo_de_garantia_plmp = pension_2_value
                elif RENTA in pension_2_concepto:
                    renta = pension_2_value
            except IndexError:
                pension_2_value = 0

            try:
                pension_3_concepto = pension_deductions[2][DESCRIPTION]
                pension_3_value = float(pension_deductions[2][IMPORT])

                if ABONO_A_PCP in pension_3_concepto:
                    abono_a_pcp = pension_3_value
                elif ABONO_A_PCV in pension_3_concepto:
                    abono_a_pcv = pension_3_value
                elif ABONO_A_PH in pension_3_concepto:
                    abono_a_ph = pension_3_value
                elif ABONO_A_PLMP in pension_3_concepto:
                    abono_a_plmp = pension_3_value
                elif ABONO_A_PMP in pension_3_concepto:
                    abono_a_pmp = pension_3_value
                elif APORTACION_A_FONDO in pension_3_concepto:
                    aportacion_a_fondo = pension_3_value
                elif FONDO_DE_GARANTIA_PCV in pension_3_concepto:
                    fondo_de_garantia_pcv = pension_3_value
                elif FONDO_DE_GARANTIA_PH in pension_3_concepto:
                    fondo_de_garantia_ph = pension_3_value
                elif FONDO_DE_GARANTIA_PLMP in pension_3_concepto:
                    fondo_de_garantia_plmp = pension_3_value
                elif RENTA in pension_3_concepto:
                    renta = pension_3_value
            except IndexError:
                pension_3_value = 0

            try:
                pension_4_concepto = pension_deductions[3][DESCRIPTION]
                pension_4_value = float(pension_deductions[3][IMPORT])

                if ABONO_A_PCP in pension_4_concepto:
                    abono_a_pcp = pension_4_value
                elif ABONO_A_PCV in pension_4_concepto:
                    abono_a_pcv = pension_4_value
                elif ABONO_A_PH in pension_4_concepto:
                    abono_a_ph = pension_4_value
                elif ABONO_A_PLMP in pension_4_concepto:
                    abono_a_plmp = pension_4_value
                elif ABONO_A_PMP in pension_4_concepto:
                    abono_a_pmp = pension_4_value
                elif APORTACION_A_FONDO in pension_4_concepto:
                    aportacion_a_fondo = pension_4_value
                elif FONDO_DE_GARANTIA_PCV in pension_4_concepto:
                    fondo_de_garantia_pcv = pension_4_value
                elif FONDO_DE_GARANTIA_PH in pension_4_concepto:
                    fondo_de_garantia_ph = pension_4_value
                elif FONDO_DE_GARANTIA_PLMP in pension_4_concepto:
                    fondo_de_garantia_plmp = pension_4_value
                elif RENTA in pension_4_concepto:
                    renta = pension_4_value
            except IndexError:
                pension_4_value = 0
            # endregion

            # region Personal
            sheet.write_number(r, 0, employee[EMPLOYEE])
            sheet.write_string(r, 1, employee[NAME])
            sheet.write_string(r, 2, employee[POSITION])
            sheet.write_string(r, 3, employee[NIVEL])
            sheet.write_string(r, 4, employee[ADSCRIPTION])
            sheet.write_string(r, 5, employee[RFC])
            sheet.write_string(r, 6, employee[CURP] if employee[CURP] else '')
            sheet.write_datetime(r, 7, employee[FECHA_ANTIGUEDAD], date_format)
            sheet.write_datetime(r, 8, end_date, date_format)
            sheet.write_string(r, 9, employee[DESCRIPCION])
            sheet.write_string(r, 10, 'Termino de administración')
            # endregion

            # region Perceptions
            quincena = employee[CALCS][QUINCENA]
            sheet.write_number(r, 11, quincena[Q_QUINCENA], money_format)
            sheet.write_number(r, 12, quincena[Q_COMPLEMENTO], money_format)
            sheet.write_number(r, 13, quincena[Q_PANTRY], money_format)
            sheet.write_number(r, 14, quincena[Q_TRANSPORT], money_format)
            sheet.write_number(r, 15, employee[CALCS][BONUS_YEAR][TOTAL], money_format)
            sheet.write_number(r, 16, employee[CALCS][PRIME][TOTAL], money_format)
            sheet.write_number(r, 17, employee[CALCS][VACATION][THIS_YEAR], money_format)
            sheet.write_number(r, 18, employee[CALCS][VACATION][LAST_YEAR], money_format)
            sheet.write_number(r, 19, employee[CALCS][FUNCTIONARY_DAY][FUNCTIONARY_DAY], money_format)

            sheet.write_number(r, 20, employee[CALCS][BONUS_YEAR][BONUS_YEAR_TAX], money_format)
            sheet.write_number(r, 21, employee[CALCS][FUNCTIONARY_DAY][FUNCTIONARY_DAY_TAX], money_format)
            sheet.write_number(r, 22, employee[CALCS][PRIME][PRIME_TAX], money_format)
            # endregion

            # region Deductions
            v_tax = employee[CALCS][VACATION][VACATION_THIS_YEAR_TAX] \
                + employee[CALCS][VACATION][VACATION_LAST_YEAR_TAX]
            sheet.write_number(r, 23, quincena[Q_QUINCENA_TAX], money_format)
            sheet.write_number(r, 24, v_tax, money_format)
            sheet.write_number(r, 25, employee[CALCS][BONUS_YEAR][BONUS_YEAR_TAX], money_format)
            sheet.write_number(r, 26, employee[CALCS][PRIME][PRIME_TAX], money_format)
            sheet.write_number(r, 27, employee[CALCS][FUNCTIONARY_DAY][FUNCTIONARY_DAY_TAX], money_format)
            # endregion

            # region Pension deductions
            sheet.write_number(r, 28, abono_a_pcp, money_format)
            sheet.write_number(r, 29, abono_a_pcv, money_format)
            sheet.write_number(r, 30, abono_a_ph, money_format)
            sheet.write_number(r, 31, abono_a_plmp, money_format)
            sheet.write_number(r, 32, abono_a_pmp, money_format)
            sheet.write_number(r, 33, aportacion_a_fondo, money_format)
            sheet.write_number(r, 34, fondo_de_garantia_pcv, money_format)
            sheet.write_number(r, 35, fondo_de_garantia_ph, money_format)
            sheet.write_number(r, 36, fondo_de_garantia_plmp, money_format)
            sheet.write_number(r, 37, renta, money_format)

            if D007 in nomina_deductions:
                sheet.write_number(r, 38, nomina_deductions[D007], money_format)
            else:
                sheet.write_number(r, 38, 0, money_format)

            if D011 in nomina_deductions:
                sheet.write_number(r, 39, nomina_deductions[D011], money_format)
            else:
                sheet.write_number(r, 39, 0, money_format)

            if D035 in nomina_deductions:
                sheet.write_number(r, 40, nomina_deductions[D035], money_format)
            else:
                sheet.write_number(r, 40, 0, money_format)

            if D042 in nomina_deductions:
                sheet.write_number(r, 41, nomina_deductions[D042], money_format)
            else:
                sheet.write_number(r, 41, 0, money_format)

            if D023 in nomina_deductions:
                sheet.write_number(r, 42, nomina_deductions[D023], money_format)
            else:
                sheet.write_number(r, 42, 0, money_format)

            if D043 in nomina_deductions:
                sheet.write_number(r, 43, nomina_deductions[D043], money_format)
            else:
                sheet.write_number(r, 43, 0, money_format)

            if D036 in nomina_deductions:
                sheet.write_number(r, 44, nomina_deductions[D036], money_format)
            else:
                sheet.write_number(r, 44, 0, money_format)

            if D028 in nomina_deductions:
                sheet.write_number(r, 45, nomina_deductions[D028], money_format)
            else:
                sheet.write_number(r, 45, 0, money_format)

            if D029 in nomina_deductions:
                sheet.write_number(r, 46, nomina_deductions[D029], money_format)
            else:
                sheet.write_number(r, 46, 0, money_format)

            if D009 in nomina_deductions:
                sheet.write_number(r, 47, nomina_deductions[D009], money_format)
            else:
                sheet.write_number(r, 47, 0, money_format)

            # endregion

            # region Totals
            calcs = employee[CALCS]
            total_p = \
                calcs[QUINCENA][Q_QUINCENA] \
                + calcs[QUINCENA][Q_COMPLEMENTO] \
                + calcs[QUINCENA][Q_PANTRY] \
                + calcs[QUINCENA][Q_TRANSPORT] \
                + calcs[BONUS_YEAR][TOTAL] \
                + calcs[PRIME][TOTAL] \
                + calcs[VACATION][LAST_YEAR] \
                + calcs[VACATION][THIS_YEAR] \
                + calcs[FUNCTIONARY_DAY][FUNCTIONARY_DAY] \
                + calcs[FUNCTIONARY_DAY][FUNCTIONARY_DAY_TAX] \
                + calcs[PRIME][PRIME_TAX] \
                + calcs[BONUS_YEAR][BONUS_YEAR_TAX]

            # The las tree elements in the sum are the subsidies.

            total_d = \
                calcs[QUINCENA][Q_QUINCENA_TAX] \
                + calcs[BONUS_YEAR][BONUS_YEAR_TAX] \
                + calcs[PRIME][PRIME_TAX] \
                + calcs[VACATION][VACATION_THIS_YEAR_TAX] \
                + calcs[VACATION][VACATION_LAST_YEAR_TAX] \
                + calcs[FUNCTIONARY_DAY][FUNCTIONARY_DAY_TAX] \
                + pension_1_value \
                + pension_2_value \
                + pension_3_value \
                + pension_4_value \
                + sum(nomina_deductions.values())

            total = total_p - total_d

            sheet.write_number(r, 48, total_p, money_format)
            sheet.write_number(r, 49, total_d, money_format)
            sheet.write_number(r, 50, total, money_format)

            sheet.write_number(r, 51, calcs[VACATION][TAKEN_LAST])
            sheet.write_number(r, 52, calcs[VACATION][TAKEN])
            sheet.write_number(r, 53, calcs[VACATION][FAULTS_LAST_YEAR])
            sheet.write_number(r, 54, calcs[VACATION][FAULTS_THIS_YEAR])
            sheet.write_number(r, 55, calcs[BONUS_YEAR][FAULTS])
            sheet.write_number(r, 56, calcs[PRIME][FAULTS])
            sheet.write_number(r, 57, calcs[FUNCTIONARY_DAY][FAULTS])
            # endregion
            r += 1
        workbook.close()
