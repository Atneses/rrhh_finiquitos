#!/usr/bin/python
# -*- coding: utf-8 -*-
import datetime
import math
import os

import pdfkit
from jinja2 import Template

from modules.connection import EMPLOYEE, NAME, FECHA_ANTIGUEDAD, CURP, RFC, ADSCRIPTION, POSITION, CALCS, BONUS_YEAR, \
    PRIME, FUNCTIONARY_DAY, VACATION, \
    THIS_YEAR, LAST_YEAR, QUINCENA, Q_QUINCENA, Q_COMPLEMENTO, Q_PANTRY, Q_TRANSPORT, \
    FUNCTIONARY_DAY_TAX, PRIME_TAX, BONUS_YEAR_TAX, Q_QUINCENA_TAX, VACATION_THIS_YEAR_TAX, VACATION_LAST_YEAR_TAX, \
    TOTAL, PENSION_DEDUCTIONS, NOMINA_DEDUCTIONS
from modules.pensions import IMPORT
from modules.utilities import float_to_text

FOLDER = 'reports/'
PDF_FILE = '{directory}{number} - {filename}-{sec}.pdf'


class SettlementReport(object):

    def __init__(self):
        with open('templates/templates.html') as template:
            self.text = template.read()

    def generate_report(self, employee, end_date):
        if not os.path.exists(FOLDER):
            os.makedirs(FOLDER)
        filename = employee[NAME]

        calcs = employee[CALCS]

        # region Get pension deductions.
        pension_total = 0
        pension_deductions = employee[PENSION_DEDUCTIONS]
        for pension in pension_deductions:
            pension[IMPORT] = float(pension[IMPORT])
            pension_total += pension[IMPORT]
        # endregion

        nomina_deductions = employee[NOMINA_DEDUCTIONS]

        total_p =\
            calcs[QUINCENA][Q_QUINCENA] \
            + calcs[QUINCENA][Q_COMPLEMENTO] \
            + calcs[QUINCENA][Q_PANTRY] \
            + calcs[QUINCENA][Q_TRANSPORT] \
            + calcs[BONUS_YEAR][TOTAL] \
            + calcs[PRIME][TOTAL] \
            + calcs[VACATION][LAST_YEAR] \
            + calcs[VACATION][THIS_YEAR] \
            + calcs[FUNCTIONARY_DAY][FUNCTIONARY_DAY] \
            + calcs[FUNCTIONARY_DAY][FUNCTIONARY_DAY_TAX] \
            + calcs[PRIME][PRIME_TAX] \
            + calcs[BONUS_YEAR][BONUS_YEAR_TAX]

        # The las tree elements in the sum are the subsidies.

        total_d =\
            calcs[QUINCENA][Q_QUINCENA_TAX]\
            + calcs[BONUS_YEAR][BONUS_YEAR_TAX]\
            + calcs[PRIME][PRIME_TAX]\
            + calcs[VACATION][VACATION_THIS_YEAR_TAX] \
            + calcs[VACATION][VACATION_LAST_YEAR_TAX]\
            + calcs[FUNCTIONARY_DAY][FUNCTIONARY_DAY_TAX]\
            + pension_total\
            + sum(nomina_deductions.values())

        total = total_p - total_d
        cents = str(math.modf(total)[0])[2:4]

        jinja_template = Template(self.text)
        text2 = jinja_template.render(
            cantidad=total
            , cantidad_letra=float_to_text(total)
            , centavos=cents
            , n_empleado=employee[EMPLOYEE]
            , nombre=employee[NAME].title()
            , departamento=employee[ADSCRIPTION].capitalize()
            , puesto=employee[POSITION].capitalize()
            , rfc=employee[RFC]
            , curp=employee[CURP]
            , fecha_antiguedad=employee[FECHA_ANTIGUEDAD].date()
            , fecha_baja=end_date
            , baja='Término de administración'
            , sueldo=calcs[QUINCENA][Q_QUINCENA]
            , complemento=calcs[QUINCENA][Q_COMPLEMENTO]
            , despensa=calcs[QUINCENA][Q_PANTRY]
            , transporte=calcs[QUINCENA][Q_TRANSPORT]
            , aguinaldo=calcs[BONUS_YEAR]['total']
            , subsidio_aguinaldo=calcs[BONUS_YEAR][BONUS_YEAR_TAX]
            , prima_vacacional=calcs[PRIME]['total']
            , subsidio_prima_vacacional=calcs[PRIME][PRIME_TAX]
            , vacaciones_2017=calcs[VACATION][LAST_YEAR]
            , vacaciones_2018=calcs[VACATION][THIS_YEAR]
            , dia_servidor=calcs[FUNCTIONARY_DAY][FUNCTIONARY_DAY]
            , subsidio_dia_servidor=calcs[FUNCTIONARY_DAY][FUNCTIONARY_DAY_TAX]
            , total_percepciones=total_p
            , isr_sueldo=calcs[QUINCENA][Q_QUINCENA_TAX]
            , isr_aguinaldo=calcs[BONUS_YEAR][BONUS_YEAR_TAX]
            , isr_prima_vacacional=calcs[PRIME][PRIME_TAX]
            , isr_dia_servidor=calcs[FUNCTIONARY_DAY][FUNCTIONARY_DAY_TAX]
            , total_deducciones=total_d
            , neto_recibir=total
            , pension_deductions=pension_deductions
            , nomina_deductions=nomina_deductions
        )

        num = datetime.datetime.now().microsecond

        pdfkit.from_string(
            text2,
            PDF_FILE.format(
                directory=FOLDER,
                number=employee[EMPLOYEE],
                filename=filename,
                sec=num
            ),
            options={'page-size': 'Legal', 'quiet': ''}
        )
